import 'package:flutter/material.dart';

class PuzzleScreen extends StatefulWidget {
  @override
  _PuzzleScreenState createState() => _PuzzleScreenState();
}

class _PuzzleScreenState extends State<PuzzleScreen> {
  
  List<Map<String,bool>> _char = [
    {'C': false}, 
    {'Y': false}, 
    {'V': false}, 
    {'Q': false}, 
    {'B': false}, 
    {'Y': false}, 
    {'A': false}, 
    {'T': false}, 
    {'O': false}, 
    {'D': false}, 
    {'S': false}, 
    {'X': false}, 
    {'W': false}, 
    {'R': false}, 
    {'F': false}, 
    {'S': false}
  ];
  
  List<String> solution = [];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double _width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Colors.black,
      body: SafeArea(
        child: Column(
          children: [
            Spacer(flex: 1,),
            Wrap(
              children: _char.map((map) {
                String key = map.keys.first;
                bool value = map.values.first;
                return GestureDetector(
                    child: Container(
                    decoration: BoxDecoration(
                      color: solution.join('') == 'WORD' && value == true ? Colors.black : value ? Colors.brown.shade700 : Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(12))
                    ),
                    width: _width*0.25,
                    height: _width*0.25,
                    child: Center(
                      child: Text(
                        key,
                        style: TextStyle(
                          color: solution.join('') == 'WORD' && value == true ? Colors.black : value ? Colors.orange.shade700 : Colors.black,
                          fontSize: 22,
                          fontWeight: FontWeight.bold
                        ),
                      ),
                    ),
                  ),
                  onTap: (){
                    if(map[key] == false && solution.length<=4){
                      setState(() {
                        map[key] = true;
                        solution.add(key);
                      });
                    }
                  }, 
                );
              }).toList(),
            ),
            Spacer(flex: 1,),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: List.generate(4,(index) => Container(
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.white),
                  borderRadius: BorderRadius.all(Radius.circular(6)),
                  color: (solution.join('') == 'WORD') ? Colors.white : Colors.transparent
                ),
                height: 30,
                width: 30,
                child: (solution.join('') == 'WORD') ? Center(child: Text(solution[index], style: TextStyle(color: Colors.black),)) : Offstage(),
              )).toList(),
            ),
            Spacer(flex: 1,),
          ],
        ),
      ),
    );
  }
}