import 'package:flutter/material.dart';
import 'package:wordpuzzle/puzzle_screen.dart';

class MainMenu extends StatefulWidget {
  MainMenu({Key key,}) : super(key: key);

  @override
  _MainMenuState createState() => _MainMenuState();
}

class _MainMenuState extends State<MainMenu> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            CustomButtonMainMenu(
              text: 'Play', 
              backgroundColor: Colors.grey.shade50, 
              borderColor: Colors.white,
              textColor: Colors.black, 
              onPressed: (){
                Navigator.push(context, MaterialPageRoute(builder: (_) => PuzzleScreen()));
              }, 
            ),
            SizedBox(height: 40,),
            CustomButtonMainMenu(
              text: 'Custom Puzzles', 
              backgroundColor: Colors.black, 
              borderColor: Colors.white,
              textColor: Colors.white, 
              onPressed: (){
                Navigator.push(context, MaterialPageRoute(builder: (_) => PuzzleScreen()));
              }, 
            )
          ],
        ),
      ),
    );
  }
}

class CustomButtonMainMenu extends StatelessWidget {
  const CustomButtonMainMenu({
    Key key,
    @required this.text,
    @required this.backgroundColor,
    @required this.borderColor,
    @required this.textColor,
    @required this.onPressed,
  }) : super(key: key);

  final String text;
  final Color backgroundColor;
  final Color borderColor;
  final Color textColor;
  final Function onPressed;

  @override
  Widget build(BuildContext context) {
    double _width = MediaQuery.of(context).size.width;
    return FlatButton(
      color: backgroundColor,
      minWidth: _width*0.9,
      padding: EdgeInsets.symmetric(vertical: 18),
      shape: StadiumBorder(
        side: BorderSide(color: borderColor, width: 2)
      ),
      child: Text(
        text,
        style: TextStyle(
          color: textColor,
          fontSize: 20,
          fontWeight: FontWeight.w600
        ),
      ),
      onPressed: onPressed, 
    );
  }
}
